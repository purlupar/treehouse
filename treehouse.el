;;; treehouse.el
;;; Makes a website out of an org-mode tree.
;;; 
;;; Copyright 2023 David Diem
;;; Contact: David Diem (treehouse-maintainer@dvdm.xyz)
;;; 
;;; Licensed under the GNU GENERAL PUBLIC LICENSE Version 3 (GPL3), a copy of which is provided in COPYING.
;;;
;;; -*- lexical-binding: t; -*-




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                           ;;
;; Enter your preferred values BELOW:                        ;;
;;                                                           ;;

(defun set-user-variables ()
  (setq siteurl "https://example.com"
        sitename "My Awesome Site"
        slogan "Just good stuff."
        logo "mylogo.jpg"
        breadcrumb-separator "❭"
        children-prefix "❭"
        exclude-from-menu "about"
        stylesheet "style.css"
        html-title '(if (equal current siteurl)
			(format "%s | %s" sitename slogan) ;; avoid redundancy on start page. (this should eventually be unified with breadcrumb generation)
			(format "%s ❬ %s | %s" current sitename slogan))		      
        logo-alt-text (format "%s -- %s" sitename slogan)
        header (format "<a href=\"%s\"><img src=\"%s/%s\" alt=\"%s\" /></a/>" siteurl siteurl logo logo-alt-text)
        footer (list (format "<a href=\"%s/more/about-me\">About</a>" siteurl))))
;;                                                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun treehouse-run (&optional n)
  "Build a directory tree from the current org-mode
buffer. Populate the directories with org-html-export pages of
subtrees"
  (interactive)
  (setq max-lisp-eval-depth (* 2 30 800)
	max-specpdl-size (* 2 15 1600))
  (set-user-variables)
  (setq project-root (list (concat (file-name-directory (buffer-file-name)) "treehouse"))
	webroot (list siteurl)
	html-head-list '(list (format "<link rel=\"icon\" href=\"%s/favicon.ico\">" siteurl)
			(format "<link rel=\"stylesheet\" type=\"text/css\" href=\"%s/%s\" />" siteurl stylesheet)
		   (format "<title>%s</title>" (eval html-title)))
;;	html-head-list '(list "a" "b" "c") ;;debugging
	html-head '(mapconcat 'identity (eval html-head-list) "<br>")
	sitemap (concat (car project-root) "/sitemap.txt")
	path (list)
	realnames (list)
	level 0)
  (shell-command (format "rm -r %s; mkdir %s" (car project-root) (car project-root)))
  (shell-command (format "rm %s; touch %s" sitemap sitemap))
  (goto-char 0)
  (org-fold-show-all)
  (setq first-dry-run t)
  (parse-buffer)
  (goto-char 0)
  (setq path (list))  
  (setq level 0)
  (setq first-dry-run nil)  
  (parse-buffer) ;; running twice (to find out what the submenus should show with 'ls', nwo that the directories are created)
  (message "Treehouse: Done."))

(defun parse-buffer ()
  (if (= (point) (point-max))
      (progn
	(message "End of file. Treehouse building succeeded!"))
    (progn
      (consume-line)
      (parse-buffer))))

(defun dir-down ()
  "Enter/Make directory from the current heading."
  (setq path (cons (read-heading) path))
  (add-to-list 'realnames (cons (webpath (read-heading)) (read-heading)))
  (setq level (+ level 1))
  (let ((pathstr (mapconcat 'identity (append project-root (mapcar 'webpath (nreverse (mapcar 'identity path)))) "/")))
    (unless (file-directory-p pathstr)
      (mkdir pathstr))
    (message pathstr)))

(defun dir-up ()
  "Remove from the path the most recently added directory."
  (pop path)
  (setq level (- level 1))
  (let ((pathstr (mapconcat 'identity (append project-root (mapcar 'webpath (nreverse (mapcar 'identity path)))) "/")))
    (message pathstr)))
  
(defun dir-flat ()
  (dir-up)
  (dir-down))

(defun export-subtree ()
  (let ((pathstr (mapconcat 'webpath (nreverse (mapcar 'identity (append path webroot))) "/")))  
    (add-url-to-sitemap pathstr))
  (let ((pathstr (mapconcat 'webpath (nreverse (mapcar 'identity (append path project-root))) "/")))
    (setq org-html-preamble (concat header (breadcrumbs) (children))
	  org-html-postamble (mapconcat 'identity footer "<br>")
	  org-html-head (eval html-head)
	  org-html-head-include-default-style nil
	  org-export-with-toc nil
	  org-html-meta-tags nil
	  org-export-with-title nil
	  org-export-with-section-numbers nil)
    (push-mark (point))
    (org-next-visible-heading 1) ;; TODO: jump to next heading of equal or higher (=fewer asterisks) level if subtree is tagged as :page:. results in the subtree with the :page: tag exporting with its children as <h2> etc. instad of folders.
    (activate-mark)
    (org-export-to-file 'html
	(concat pathstr "/index.html") nil nil)
    (deactivate-mark)          
    (message "New page: %s/index.htm)" pathstr)
    
    (fix-empty-html-title pathstr)))

(defun add-url-to-sitemap (url)
  (write-region (format "%s
" url) nil sitemap 'append))


(defun build-dir ()
  (if (= (detect-level) level)
      (dir-flat)
    (progn
      (while (> (detect-level) level)
	(dir-down))
      (if (< (detect-level) level)
	  (progn
	    (while (< (detect-level) level)
	      (dir-up))
	    (dir-flat)))))
  (forward-line))

(defun consume-line ()
  (if (looking-at "\*+ ")
      (build-dir)
    (forward-line -1)
    (if first-dry-run
	(org-next-visible-heading 1)
      (export-subtree))))

(defun detect-level ()
  "In a headline, count asterisks at beginning of line."
  (count-matches "\*" (line-beginning-position) (save-excursion (progn (goto-char (line-beginning-position)) (search-forward-regexp " " (+ (point) 10))))))

(defun read-heading ()
  "Read the heading text (strip asterisks) in the current line."
  (goto-char (line-beginning-position))
  (buffer-substring-no-properties (search-forward-regexp " " (+ (point) 10)) (line-end-position)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                                             ;;
;; HTML Generating functions below ...                                                         ;;
;;                                                                                             ;;

(defun breadcrumbs ()
  (setq crumbs (mapcar 'identity (append path webroot)))
  (setq rich (list))
  (setq current (car crumbs))
  (setq crumbs (cdr crumbs))
  (let ((url (mapconcat 'identity (nreverse (mapcar 'identity crumbs)) "/")))
    (setq rich (cons
		(format "<span class=\"current\">%s</span>" (if (equal (list current) webroot) sitename current))
		rich)))
  (while crumbs
    (let ((url (mapconcat 'webpath (nreverse (mapcar 'identity crumbs)) "/")))
      (setq crumb (pop crumbs))
      (setq linktext (if (equal (list crumb) webroot) sitename crumb))
      (setq rich (cons
		  (format "<a href=\"%s\">%s</a>"
			  url
			  linktext)
		  rich))))
  (concat "<nav><div id=\"menu\">" (mapconcat 'identity rich (format " %s " breadcrumb-separator)) "</div>"))

(defun children ()
  (let ((currentdir (mapconcat 'identity (append project-root (mapcar 'webpath (nreverse (mapcar 'identity path)))) "/")))
    (setq children (butlast (split-string (shell-command-to-string (format "ls %s --hide index.html --hide %s" currentdir exclude-from-menu)) "\n"))))
  (setq currentdir (mapconcat 'identity (nreverse (mapcar 'identity (append path webroot))) "/"))
  (setq rich (list))
  (when children
      (while children
	(let ((elt (pop children)))
	  (setq url (webpath (concat currentdir "/" elt)))
	  (setq rich (cons
		      (format " %s <a href=\"%s\">%s</a>" children-prefix url (or (cdr (assoc elt realnames)) elt)) rich))))
(concat "<div id=\"submenu\">" (mapconcat 'identity rich "<br>") "</div></nav>")))

(defun webpath (s &optional inhibitdowncase)
  (setq s (replace-regexp-in-string "(" "" s))
  (setq s (replace-regexp-in-string ")" "" s))
  (setq s (replace-regexp-in-string " " "-" s))
  (setq s (replace-regexp-in-string "ä" "ae" s))
  (setq s (replace-regexp-in-string "ö" "oe" s))
  (setq s (replace-regexp-in-string "ü" "ue" s))
  (setq s (replace-regexp-in-string "ß" "ss" s))
  (if (not (equal (list s) project-root))
      (if (not inhibitdowncase)
	  (downcase s)
	s)
    s))

(defun fix-empty-html-title (somepathstr)
  (let ((htmlfile (concat somepathstr "/index.html")))
    (with-temp-buffer (insert-file-contents htmlfile)
		      (goto-char 0)
		      (setq linecount (count-lines (point-min) (point-max)))
		      (while (and (not (looking-at "<title>&lrm;</title>"))
				  (not (= linecount (line-number-at-pos))))
			(forward-line))
		      (if (looking-at "<title>&lrm;</title>")
			  (kill-line))
		      (write-region (point-min) (point-max) htmlfile))))
